import { mount } from "@vue/test-utils";
import Parent from "@/components/Parent.vue";

describe("Parent.vue", () => {
    it("Is text present", ()=> {
        const wrapper = mount(Parent);

        expect(wrapper.text()).toContain("Child");
    })
});

// mount = Will render all component
// but shallowMount will only focus on current working component

