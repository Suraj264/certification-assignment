// Import the necessary dependencies for unit testing Vue components with Jest
import { shallowMount } from '@vue/test-utils';
import BookDeal from '@/modules/BookDeal/BookDeal.vue'; // Import the Vue component to be tested

describe('BookDeal.vue', () => {
  let wrapper;

  beforeEach(() => {
    // Create a shallow mount of the BookDeal component before each test
    wrapper = shallowMount(BookDeal);
  });

  afterEach(() => {
    // Destroy the wrapper after each test to clean up memory
    wrapper.destroy();
  });

  it('renders correctly', () => {
    // Assert that the component is rendered correctly
    expect(wrapper.exists()).toBe(true);
  });

  it('updates selectedRecords when bookDealEntered method is called', () => {
    // Update the BOOK_DEAL data property
    wrapper.setData({ BOOK_DEAL: 'TransType Currency Tenor 100K' });

    // Call the bookDealEntered method
    wrapper.vm.bookDealEntered();

    // Assert that the selectedRecords data property is updated correctly
    expect(wrapper.vm.selectedRecords.TRANS_TYPE).toBe('TransType');
    expect(wrapper.vm.selectedRecords.CURRENCY).toBe('Currency');
    expect(wrapper.vm.selectedRecords.TENOR).toBe('Tenor');
    expect(wrapper.vm.selectedRecords.AMOUNT).toBe(100000);
  });

  it('adds data to items when addDataToGrid method is called', () => {
    // Update the selectedRecords data property
    wrapper.setData({
      selectedRecords: {
        TRANS_TYPE: 'TransType',
        CURRENCY: 'Currency',
        TENOR: 'Tenor',
        AMOUNT: 100000,
      },
    });

    // Call the addDataToGrid method
    wrapper.vm.addDataToGrid();

    // Assert that the data is added to the items data property
    expect(wrapper.vm.items.length).toBe(1);
    expect(wrapper.vm.items[0].TRANS_TYPE).toBe('TransType');
    expect(wrapper.vm.items[0].CURRENCY).toBe('Currency');
    expect(wrapper.vm.items[0].TENOR).toBe('Tenor');
    expect(wrapper.vm.items[0].AMOUNT).toBe(100000);
  });
});