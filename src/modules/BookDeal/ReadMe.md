## Book Deal

- Template Section:
    
    - Includes a form group with an input field for "Book Deal" which is bound to the BOOK_DEAL data property using v-model directive.
    - It also includes form groups with input fields for "Trans Type", "Currency", "Tenor", and "Amount" which are bound to respective data properties in the selectedRecords object.
    - It also includes a table component from the Bootstrap-Vue library to display entered data.

- Script Section:
    - The script section defines the Vue.js component with the name "book-deal".
    - It includes a data object that defines the initial data properties for the component, such as BOOK_DEAL, selectedRecords, fields, and items.
    - It also includes methods object that defines the methods for handling user interactions, such as bookDealEntered which splits the entered "Book Deal" value into separate parts and updates the corresponding data properties, and addDataToGrid which adds the entered data to the items array to display in the table.

- Working 
  - When you enter a book deal in `Book Deal` input field on blur of this field tran_type, currency, tenor and amount gets generated.
  - As soon as you click on `Book Deal` button the records get inserted into grid